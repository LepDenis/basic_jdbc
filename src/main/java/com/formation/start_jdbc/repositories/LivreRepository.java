package com.formation.start_jdbc.repositories;

import java.util.List;
import java.util.Optional;

import com.formation.start_jdbc.dao.Crud;
import com.formation.start_jdbc.modeles.Livre;

public interface LivreRepository extends Crud<Livre, String> {

	Optional<List<Livre>> findByTitle(String title);
}
