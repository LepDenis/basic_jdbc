package com.formation.start_jdbc.services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import com.formation.start_jdbc.dao.ConnexionDb;
import com.formation.start_jdbc.modeles.Livre;
import com.formation.start_jdbc.repositories.LivreRepository;

public class LivreService implements LivreRepository {

	@Override
	public Optional<Collection<Livre>> findAll() {

		Collection<Livre> liste = new ArrayList<>();
		String query = "select * from livre";

		try (PreparedStatement ps = ConnexionDb.getInstance().prepareStatement(query);
				ResultSet rs = ps.executeQuery();) {
			while (rs.next()) {
				Livre livre = new Livre();
				livre.setIsbn(rs.getString(1));
				livre.setTitre(rs.getString(2));
				livre.setAuteurNom(rs.getString(3));
				livre.setAuteurPrenom(rs.getString(4));
				livre.setEditeur(rs.getString(5));
				livre.setAnnee(rs.getInt(6));
				liste.add(livre);
			}
			return Optional.ofNullable(liste);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Optional.empty();

	}

	@Override
	public Optional<Livre> findById(String id) {

		Livre livre = null;
		String query = "select * from livre where isbn = ?;";

		try (PreparedStatement ps = ConnexionDb.getInstance().prepareStatement(query);) {
			ps.setString(1, id);
			try (ResultSet rs = ps.executeQuery();) {
				if (rs.next()) {
					livre = new Livre();
					livre.setIsbn(rs.getString(1));
					livre.setTitre(rs.getString(2));
					livre.setAuteurNom(rs.getString(3));
					livre.setAuteurPrenom(rs.getString(4));
					livre.setEditeur(rs.getString(5));
					livre.setAnnee(rs.getInt(6));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Optional.ofNullable(livre);
	}

	@Override
	public int save(Livre livre) {

		String query = (findById(livre.getIsbn()).isEmpty())
				? "insert into livre (titre, auteur_nom, auteur_prenom, editeur, annee, isbn) values (?,?,?,?,?,?);"
				: "update livre set titre = ?, auteur_nom = ?, auteur_prenom = ?, editeur = ?, annee = ? where isbn = ?;";

		try (PreparedStatement ps = ConnexionDb.getInstance().prepareStatement(query);) {

			ps.setString(1, livre.getTitre());
			ps.setString(2, livre.getAuteurNom());
			ps.setString(3, livre.getAuteurPrenom());
			ps.setString(4, livre.getEditeur());
			ps.setInt(5, livre.getAnnee());
			ps.setString(6, livre.getIsbn());

			return ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int delete(Livre livre) {
		return this.deleteById(livre.getIsbn());
	}

	@Override
	public int deleteById(String id) {

		String query = "delete from livre where isbn = ?;";

		try (PreparedStatement ps = ConnexionDb.getInstance().prepareStatement(query);) {
			ps.setString(1, id);
			return ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public Optional<List<Livre>> findByTitle(String title) {

		List<Livre> liste = new ArrayList<>();
		String query = "select * from livre where titre like ?";

		try (PreparedStatement ps = ConnexionDb.getInstance().prepareStatement(query);) {
			ps.setString(1, "%" + title + "%");
			try (ResultSet rs = ps.executeQuery();) {
				while (rs.next()) {
					Livre livre = new Livre();
					livre.setIsbn(rs.getString(1));
					livre.setTitre(rs.getString(2));
					livre.setAuteurNom(rs.getString(3));
					livre.setAuteurPrenom(rs.getString(4));
					livre.setEditeur(rs.getString(5));
					livre.setAnnee(rs.getInt(6));
					liste.add(livre);
				}
				return Optional.ofNullable(liste);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Optional.empty();
	}

}
