package com.formation.start_jdbc.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class ConnexionDb {

	private static Connection conn;
	private static String url;
	private static String login;
	private static String password;

	private ConnexionDb() {
	}

	private static void loadProps(String fileName) {

		Properties props = new Properties();
		try (FileInputStream fis = new FileInputStream(fileName)) {
			props.load(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}

		url = props.getProperty("jdbc.url");
		login = props.getProperty("jdbc.login");
		password = props.getProperty("jdbc.password");
	}

	public static Connection getInstance() {

		if (conn == null) {
			loadProps("conf.properties");
			try {
				conn = DriverManager.getConnection(url, login, password);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return conn;
	}

}
