package com.formation.start_jdbc.dao;

import java.util.Collection;
import java.util.Optional;

public interface Crud<M, I> {

	Optional<Collection<M>> findAll();
	
	Optional<M> findById(I id);

	int save(M object);

	int delete(M object);

	int deleteById(I id);
}
