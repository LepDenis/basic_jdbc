package com.formation.start_jdbc;

import com.formation.start_jdbc.modeles.Livre;
import com.formation.start_jdbc.services.LivreService;

public class StartJdbc {

	public static void main(String[] args) {

		LivreService serv = new LivreService();
		Livre livre = new Livre("978-2070368228", "1984", "Orwell", "George", "Gallimard", 2015);

		// serv.save(livre);

		serv.findByTitle("1984").ifPresentOrElse(liste -> liste.forEach(System.out::println),
				() -> System.out.println("La liste vaut null !"));

	}

}
