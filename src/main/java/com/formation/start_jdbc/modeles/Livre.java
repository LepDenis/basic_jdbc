package com.formation.start_jdbc.modeles;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = { "isbn", "titre", "auteurPrenom", "auteurNom", "editeur", "annee" })
public class Livre {

	String isbn;
	String titre;
	String auteurNom;
	String auteurPrenom;
	String editeur;
	int annee;

}
